set GLOBAL event_scheduler = ON;

create schema
if not exists
    FORUMDB;

grant usage on FORUMDB.* to 'FORUMDBroot'@'localhost';
drop user
    'FORUMDBroot'@'localhost';
create user
    'FORUMDBroot'@'localhost'
    identified by password '';

use FORUMDB;

--- USER ACCOUNT MANAGEMENT ---
--- table declarations
create table
if not exists
    USERS (
        id      int         auto_increment,
        
        login   varchar(50) not null unique,
        passw   varchar(256)not null,
        role    enum(
                'DEFAULT',
                'MODERATOR',
                'ADMIN'),
        status  enum(
                'PENDING',
                'ACTIVE',  
                'BLOCKED',
                'CLOSED'), 
        points  int,
        created datetime,

        primary key (id)
    );
create table
if not exists
    USERDATA (
        id      int         auto_increment,

        user    int         not null unique,
        
        name    varchar(120), 
        service varchar(120), 
        pubkey  varbinary(4096),
        
        primary key (id),
        foreign key (user)
            references USERS(id)
            on update CASCADE
            on delete CASCADE
    );
--- view declarations
create or replace view
    USERS_PENDING as
        select id,login,role,created
        from  USERS
        where status = 'PENDING';
create or replace view
    USERS_ACTIVE  as
        select id,login,role,created
        from  USERS
        where status = 'ACTIVE';
create or replace view
    USERS_BLOCKED as
        select id,login,role,created
        from  USERS
        where status = 'BLOCKED';
create or replace view
    USERS_CLOSED  as
        select id,login,role,created
        from  USERS
        where status = 'CLOSED';
--- automatic value assignments
delimiter //
drop trigger if exists BEFORE_USERS_INSERT;//
create trigger BEFORE_USERS_INSERT
    before insert
    on USERS
    for each row
    begin
        set new.role    = 'DEFAULT';
        set new.status  = 'PENDING';
        set new.points  = 0;
        set new.created = now();
    end //
drop trigger if exists AFTER_USERS_INSERT;//
create trigger AFTER_USERS_INSERT
    after insert
    on USERS
    for each row
    begin
        insert into USERDATA(user)
            values(new.id);
    end //
delimiter ;
--- lifetime management
--delimiter //
create event
if not exists
    USERS_DELETECLOSED
    on schedule every 2 DAY
    do
        delete from USERS_CLOSED;
create event
if not exists
    USERS_DELETEPENDING
    on schedule every 12 HOUR
    do
        delete from USERS_PENDING
            where timestampdiff(HOUR,
                                created,
                                NOW()) >= 48;
--delimiter ;

--- CONTENT MANAGEMENT ---
--- table declarations
create table
if not exists
    THREADS (
        id      int     auto_increment,
        
        moder   int,
        status  enum(
                'ACTIVE',   
                'CLOSED',    
                'ARCHIVED'), 

        title   varchar(120),
        lastact datetime, 

        primary key (id),
        foreign key (moder)
            references USERS(id)
            on update CASCADE
            on delete SET NULL
    );
create table
if not exists
    POSTS (
        id      int     auto_increment,
        
        thread  int     not null,
        author  int,

        content text    not null,
        posted  datetime,
        
        primary key (id),
        foreign key (thread)
            references THREADS(id)
            on update CASCADE
            on delete CASCADE,
        foreign key (author)
            references USERS(id)
            on update CASCADE
            on delete SET NULL
    );
--- view declarations
create or replace view
    THREADS_ARCHIVED as
        select id,title,lastact
        from  THREADS
        where status = 'ARCHIVED';
--- automatic value assignment
delimiter //
drop trigger if exists BEFORE_THREADS_INSERT;//
create trigger BEFORE_THREADS_INSERT
    before insert
    on THREADS
    for each row
    begin
        set new.status  = 'ACTIVE';
        set new.lastact = now();
    end //
drop trigger if exists BEFORE_POSTS_INSERT;//
create trigger BEFORE_POSTS_INSERT
    before insert
    on POSTS
    for each row
    begin
        declare status varchar(10);
        select t.status
            from  THREADS as t
            where t.id = new.thread
            into  status;
        if not status = 'ACTIVE' then
            signal sqlstate '45000'
                set message_text = 'Thread closed';
        end if;
        set new.posted = now();
        update THREADS
            set   lastact = new.posted
            where id = new.thread;
    end //
delimiter ;
--- lifetime management
--delimiter //
create event
if not exists
    THREADS_DELETEARCHIVED
    on schedule every 1 MONTH
    do
        delete from THREADS_ARCHIVED
            where timestampdiff(YEAR,
                                lastact,
                                now()) >= 2;
create event
if not exists
    THREADS_ARCHIVEINACTIVE
    on schedule every 1 MONTH
    do
        update THREADS
            set   status = 'ARCHIVED'
            where status = 'ACTIVE' and
                  timestampdiff(YEAR,
                                lastact,
                                now()) >= 1;
--delimiter ;

grant all privileges
    on FORUMDB.*
    to 'FORUMDBroot'@'localhost';

flush privileges;

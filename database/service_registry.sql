set GLOBAL event_scheduler = ON;

create schema
if not exists
    SERVDB;

grant usage
    on SERVDB.*
    to 'SERVDBroot'@'localhost';
drop user
    'SERVDBroot'@'localhost';
create user
    'SERVDBroot'@'localhost'
    identified by password '';

grant usage
    on SERVDB.*
    to 'request'@'%';
drop user
    'request'@'%';
create user
    'request'@'%'
    identified by password '';

use SERVDB;

--- RELAY LIST MANAGEMENT ---
--- table declarations
create table
if not exists
    RELAYS (
        id      int          auto_increment,
        service varchar(120) not null unique,
        status  bit(1)       not null default 1,
        lastact datetime,
        constraint id_relay
            primary key(id)
    );
--- view declarations
create or replace view
    RELAYS_ACTIVE as
        select service
            from  RELAYS
            where status = 1;
create or replace view
    RELAYS_INACTIVE as
        select id,lastact
            from  RELAYS
            where status = 0;
--- external interaction API
--delimiter //
drop procedure
if exists
    RELAYS_DEACTIVATE;
create procedure RELAYS_DEACTIVATE(service varchar(120))
    update RELAYS as r
        set   r.status  = 0,
              r.lastact = now()
        where r.service like service;
drop procedure
if exists
    RELAYS_ACTIVATE;
create procedure RELAYS_ACTIVATE(service varchar(120))
    update RELAYS as r
        set   r.status = 1
        where r.service like service;
--delimiter ;
--- lifetime management
--delimiter //
create event
if not exists
    RELAYS_DELETEINACTIVE
    on schedule every 5 MINUTE
    do
        delete from RELAYS_INACTIVE
            where timestampdiff(MINUTE,
                                lastact,
                                now()) >= 5;
--delimiter ;

grant all privileges
    on   SERVDB.*
    to   'SERVDBroot'@'localhost';

grant select
    on   SERVDB.RELAYS_ACTIVE
    to   'request'@'%';

flush privileges;

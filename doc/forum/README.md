# Distortion(c) - Distributed Market

## Support forum

### Table of contents

- [about](#about)
  - [community](#community)
- [usage](#modus-operandi)
- [infrastructure](#infrastructure)

### About

#### community

An obscenely obvious observation is that no network of trust can sustain itself
and fulfill its purpose without gathering a tightly-knit, dedicated community of
participants. To this end we seek to provide an universally accessible platform 
for sharing advice, opinion and suggestions regarding possible future course of 
development. As **Distortion Forum** is a Web-oriented solution, its accounts are 
*by no means* connected to your [**Distortion Market client**](https://www.gitlab.com/distributed_market/client) 
(unless you willfully decide otherwise) so that under no circumstances
shall your privacy be compromised.

### Modus operandi

Main article: [*data structures*](./DATA_STRUCTURES.md)

**Distortion Forum** allows its users to create and manage *User Accounts*, which
in turn they can utilize to create \(and view\) *Threads* and *Posts*. For the sake
of maintaining high discussion level each *Thread* is supervised by a *Moderator*,
whose job is to preserve it from straying too far out into the traitorous seas of 
digression using direct coercion \(*Post* deletion, *User Account* suspension\).
Each *Moderator* can also request that a particularly delinquent *User Account*
be deleted by an *Administrator*, whose privileges and capabilites match that of
a minor god at least.

### Infrastructure

As of v0.0.1 up to v1.0.0 **Distortion Server** will be hosted using AWS, 
following pattern:
- front-end
  - one instance
  - exposed to remote connections
  - hosts user interfaces
- forum back-end
  - one instance
  - only accepts from front-end
  - hosts forum logic
- relay provider
  - three instances
  - only accepts from front-end
  - each hosts database copy
  - load-balanced

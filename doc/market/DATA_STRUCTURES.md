# Distortion(c) - Distributed Market

## Server Data Structures

### Table of Contents

- [about](#about)
- [sql objects](#sql-hierarchy)
- [sql security](#sql-security)

### About

Internal database on provider instances is extremally simplified - it consists 
of but one table and two views, and is configured to work with two distinct 
user accounts that differ in privilege level. It's implementation uses 
[MySQL v5.5](https://dev.mysql.com/doc/refman/5.5/en/).

### SQL hierarchy

1. **Tables**
   1. **RELAYS**
      - **id**:      internally managed primary key
      - **service**: [Tor hidden service](https://2019.www.torproject.org/docs/onion-services) address
      - **status**:  relay activity status
      - **lastact**: timestamp specifying when the relay has last been active
2. **Views**
   1. **RELAYS\_ACTIVE**
      - *service* of **RELAYS** with positive *status*
   2. RELAYS\_INACTIVE
      - *lastact* of **RELAYS** with negative *status*
3. **Procedures**
   1. **RELAY\_ACTIVATE**
      - set *status to* **1**
   2. **RELAY\_DEACTIVATE**
      - set *status* to **0**
      - set *lastact* to **NOW()**
4. **Events**
   1. **RELAY\_DELETEINACTIVE**
      - periodic (5 min)
      - deletes **RELAYS** inactive for >=5 min

### SQL security

Database recognizes two users:
- **SERVDBroot@localhost**
  - all privileges
  - symbolizes admin access/local logic
- **request@\<front-end instance IP\>**
  - can select from RELAYS\_ACTIVE
  - symbolizes request from client, passed via front-end instance

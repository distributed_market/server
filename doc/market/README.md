# Distortion(c) - Distributed Market

## Central server

### Table of contents

- [about](#about)
  - [description](#how-does-it-work)
  - [purpose](#but-why-do-we-need-a-central-server)
- [querying](#rest-api)
- [infrastructure](#infrastructure)

### About

#### how does it work?

[**Distortion Server**](https://www.gitlab.com/distributed_market/server) 
assembles and maintains a registry of 
[**Distortion Market**](https://www.gitlab.com/distributed_market/client) 
[*relays*](https://www.gitlab.com/distributed_market/client/doc/relay/README.md), 
providing a convenient way for REST-ful querying of 
[*Tor hidden service*](https://2019.www.torproject.org/docs/onion-services) 
database (holding addresses of said relays). Registry is updated real-time as 
relays go on- and offline (offline meaning not answering for 5-10min straight) 
and distributed between several nodes for reliability, accessed via a single 
front-end node that also provides user interface for 
[**Distortion Forum**](https://www.gitlab.com/distributed_market/server/doc/forum/README.md).

#### ...but why do we need a central server?

Considering the nature of **Distortion Market** one may ask - and would be 
perfectly rightful to - whether clients' communication with a SPoF of any kind, 
let alone one that makes for a target of so many attack vectors, does not 
undermine the whole privacy-first, safety-oriented Web-of-Trust idea. It should 
however be noted that **Distortion Server** only serves the purpose of client-relay 
communication simplification, and that for added security one may still configure 
their client to utilize another relay service distributor, or even to not use any 
provider at all (which probably requires obtaining relay address directly from 
the hoster); just as a relay can be configured not to signalize status change to 
remote factors.

### REST API

Main article: [*data structures*](./DATA_STRUCTURES.md)

**Distortion Server** reacts to REST queries from any source and responds with 
JSON array containing a set of N service strings to be tried by a client. It 
temporarily stores query source and creates an associated iterator, so that each 
consecutive query returns a different set of results. It also accepts special 
*REGISTER\_RELAY* query, storing query source in the database.

### Infrastructure

As of v0.0.1 up to v1.0.0 **Distortion Server** will be hosted using AWS, 
following pattern:
- front-end
  - one instance
  - exposed to remote connections
  - hosts user interfaces
- forum back-end
  - one instance
  - only accepts from front-end
  - hosts forum logic
- relay provider
  - three instances
  - only accepts from front-end
  - each hosts database copy
  - load-balanced
